//
//  MasterViewController.swift
//  iOS Offline Sync POC
//
//  Created by Toine Bakkeren on 09-04-16.
//  Copyright © 2016 Avans. All rights reserved.
//

import Haneke
import SwiftyJSON
import UIKit

class MasterViewController: UITableViewController {
    var data:[Record] = []
    let cache = Shared.JSONCache;
    let URL = NSURL(string: "https://api.discogs.com/users/soundman12/collection/folders/0/releases?page=1&per_page=100")!;

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        cache.fetch(URL: URL).onSuccess { response in
            let responseSwifty = SwiftyJSON.JSON(data: response.asData())
            let dataObjects = responseSwifty["releases"].arrayValue
            
            for item in dataObjects {
                let newRecord = Record(title: item["basic_information"]["title"].string!, artist: item["basic_information"]["artists"][0]["name"].string!, year: item["basic_information"]["year"].int!)
                    self.data.append(newRecord)
            }
            
            self.tableView.reloadData()
        }
        
        // Do any additional setup after loading the view, typically from a nib.
        //self.navigationItem.leftBarButtonItem = self.editButtonItem()

        //let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        //self.navigationItem.rightBarButtonItem = addButton
        
        
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertNewObject(sender: AnyObject) {
        //objects.insert(NSDate(), atIndex: 0)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "TableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TableViewCell
        
        let object = data[indexPath.row]
        print(object.title)
        
        let begin: String = object.artist + " - " + object.title
        let fieldText: String = begin + " (" + object.year.description + ")"
        cell.TitleField.text = fieldText
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //data.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

