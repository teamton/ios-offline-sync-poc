//
//  TableViewCell.swift
//  iOS Offline Sync POC
//
//  Created by Toine Bakkeren on 09-04-16.
//  Copyright © 2016 Avans. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var TitleField: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
