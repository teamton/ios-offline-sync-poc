//
//  Record.swift
//  iOS Offline Sync POC
//
//  Created by Toine Bakkeren on 09-04-16.
//  Copyright © 2016 Avans. All rights reserved.
//

import UIKit

class Record {
    var title: String
    var artist: String
    var year: Int
    
    init(title: String, artist: String, year: Int) {
        self.artist = artist;
        self.title = title;
        self.year = year;
    }
    
}

